﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", MainLauncher = true, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Main : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);

            if (Helper.MySystem == null)
            {
                Helper.MySystem = "0";
            }

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            FindViewById<Button>(Resource.Id.btnLogin).Click += OnLoginClick;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnConnectionsSettingsClick;

            // Add version to the page
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);
        }

        private void OnLoginClick(object sender, System.EventArgs e)
        {
            try
            {
                Intent intent = new Intent(this, typeof(LogIn));
                StartActivityForResult(intent, 1);
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnConnectionsSettingsClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(SettingsConnect));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if ((requestCode == 1) & (resultCode == Result.Ok))
            {
                StartActivity(typeof(Menu));
            }
        }
    }
}

