﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MenuPick : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.MenuPick);

            FindViewById<Button>(Resource.Id.buttonPickPush).Click += OnButtonPickPush_Click;
            
            FindViewById<Button>(Resource.Id.buttonPickNewStore).Click += OnButtonPickNewStore_Click;
            
            FindViewById<Button>(Resource.Id.buttonPickCallOff).Click += OnButtonPickCallOff_Click;
            
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();

            // Add version to the page
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);

            if (Helper.Demo)
            {
                FindViewById<Button>(Resource.Id.buttonPickPush).Visibility = ViewStates.Invisible;
                FindViewById<Button>(Resource.Id.buttonPickNewStore).Visibility = ViewStates.Invisible;
            }
        }

        public void OnButtonPickPush_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(PickPushOrderList));
        }

        private void OnButtonPickNewStore_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(PickNewStoreOrderList));
        }

        private void OnButtonPickCallOff_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(PickCallOffOrderList));
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}