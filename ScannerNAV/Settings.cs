﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Views;
using Android.OS;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "Settings", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Settings : Activity
    {
        private String ResponseTitle = "";
        private String ResponseMassage = "";
        private zone[] zoneList;
        private List<string> zoneItems;
        private int selectedZoneItem = 0;
        private printer_group[] printerList;
        private List<string> printerItems;
        private int selectedPrinterItem = 0;
        private bool cbShowAll;
        private bool cbCheckInput;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.Settings);

            FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Visible;
            FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Gone;

            FindViewById<Button>(Resource.Id.btnSave).Click += OnSave;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {
            
            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                FindViewById<Spinner>(Resource.Id.spnZone).Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, zoneItems);
                FindViewById<Spinner>(Resource.Id.spnZone).SetSelection(selectedZoneItem);
                FindViewById<Spinner>(Resource.Id.spnPrinterGroup).Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, printerItems);
                FindViewById<Spinner>(Resource.Id.spnPrinterGroup).SetSelection(selectedPrinterItem);
            }
            else
            {
                Helper.ShowAlertDialog(this, ResponseTitle, ResponseMassage);
            }
            
            FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Visible;
            FindViewById<CheckBox>(Resource.Id.cbShowAll).Checked = cbShowAll;
            FindViewById<CheckBox>(Resource.Id.cbCheckInput).Checked = cbCheckInput;
        }

        private Boolean LoadData()
        {
            try
            {
                if (Helper.Demo)
                {
                    FindViewById<TextView>(Resource.Id.tvResourceNo).Text = Helper.RescourceNo;
                    FindViewById<TextView>(Resource.Id.tvName).Text = Helper.RescourceNo;
                    FindViewById<TextView>(Resource.Id.tvDistNo).Text = "12345|23456|34567|45678";
                    cbShowAll = true;
                    cbCheckInput = true;

                    zoneItems = new List<string>(5)
                    {
                        "ZONE1" + " - " + "Zone 1",
                        "ZONE2" + " - " + "Zone 2",
                        "ZONE3" + " - " + "Zone 3",
                        "ZONE4" + " - " + "Zone 4",
                        "ZONE5" + " - " + "Zone 5"
                    };
                    selectedZoneItem = 1;

                    printerItems = new List<string>(3)
                    {
                        "PRINTER1" + " - " + "Printer 1",
                        "PRINTER2" + " - " + "Printer 2",
                        "PRINTER3" + " - " + "Printer 3"
                    };
                    selectedPrinterItem = 2;
                }
                else
                {
                    resource_root resourceRoot = new resource_root();
                    ScannerInterface ws = Helper.GetInterface(this);
                    ws.GetResourcesInfo(ref resourceRoot, Helper.RescourceNo);
                    resource_response resourceInfoXmlResponse = resourceRoot.resource_response[0];

                    if (Helper.IsOK(resourceInfoXmlResponse.status))
                    {
                        resource resourceInfo = resourceInfoXmlResponse.resource[0];
                        FindViewById<TextView>(Resource.Id.tvResourceNo).Text = resourceInfo.no;
                        FindViewById<TextView>(Resource.Id.tvName).Text = resourceInfo.name;
                        FindViewById<TextView>(Resource.Id.tvDistNo).Text = resourceInfo.distno;
                        cbShowAll = resourceInfo.pickall != "0";
                        cbCheckInput = resourceInfo.checkinput != "0";

                        // Zones
                        zone_root zone_Root = new zone_root();
                        ws.GetZone(ref zone_Root);
                        zone_response zoneXmlResponse = zone_Root.zone_response[0];
                        if (Helper.IsOK(zoneXmlResponse.status))
                        {
                            int counter = 0;
                            zoneItems = new List<string>(zoneXmlResponse.zone.Length);
                            zoneList = zoneXmlResponse.zone;
                            foreach (zone item in zoneXmlResponse.zone)
                            {
                                zoneItems.Add(item.zone_code + " - " + item.zone_desc);
                                if (item.zone_code == resourceInfo.zone)
                                {
                                    selectedZoneItem = counter;
                                }
                                counter++;
                            }
                        }
                        else
                        {
                            ResponseTitle = zoneXmlResponse.status;
                            ResponseMassage = zoneXmlResponse.status_text;
                            return false;
                        }

                        // Printers
                        printergroup_root printer_Root = new printergroup_root();
                        ws.GetPrinterGroup(ref printer_Root);
                        printergroup_response printerXmlResponse = printer_Root.printergroup_response[0];
                        if (Helper.IsOK(printerXmlResponse.status))
                        {
                            int counter = 0;
                            printerItems = new List<string>(printerXmlResponse.printer_group.Length);
                            printerList = printerXmlResponse.printer_group;
                            foreach (printer_group item in printerXmlResponse.printer_group)
                            {
                                printerItems.Add(item.printer_no + " - " + item.printer_desc);
                                if (item.printer_no == resourceInfo.printergroup)
                                {
                                    selectedPrinterItem = counter;
                                }
                                counter++;
                            }
                        }
                        else
                        {
                            ResponseTitle = printerXmlResponse.status;
                            ResponseMassage = printerXmlResponse.status_text;
                            return false;
                        }
                    }
                    else
                    {
                        ResponseTitle = resourceInfoXmlResponse.status;
                        ResponseMassage = resourceInfoXmlResponse.status_text;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ResponseTitle = "ERROR";
                ResponseMassage = ex.Message;
                return false;
            }
            return true;
        }

        private void OnSave(object sender, EventArgs e)
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.Zone = zoneItems[FindViewById<Spinner>(Resource.Id.spnZone).SelectedItemPosition].Substring(0, 5);
                    SetResult(Result.Ok);
                    Finish();
                }
                else
                {
                    default_root defaultResponseRoot = new default_root();
                    ScannerInterface ws = Helper.GetInterface(this);
                    ws.SetResourcesInfo(ref defaultResponseRoot, Helper.RescourceNo, FindViewById<CheckBox>(Resource.Id.cbShowAll).Checked, zoneList[FindViewById<Spinner>(Resource.Id.spnZone).SelectedItemPosition].zone_code, printerList[FindViewById<Spinner>(Resource.Id.spnPrinterGroup).SelectedItemPosition].printer_no, FindViewById<CheckBox>(Resource.Id.cbCheckInput).Checked);
                    default_response XmlResponse = defaultResponseRoot.default_response[0];
                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.Zone = zoneList[FindViewById<Spinner>(Resource.Id.spnZone).SelectedItemPosition].zone_code;
                        Helper.CheckInput = FindViewById<CheckBox>(Resource.Id.cbCheckInput).Checked;
                        SetResult(Result.Ok);
                        Finish();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }
    }
}