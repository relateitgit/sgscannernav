﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "CountPallet")]
    public class CountBinItemNo : Activity
    {
        private string itemno;
        private DateTime newDate;
        private DateTime today;
        private List<string> BoxTypes;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            LoadView();
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.CountBinItemNo);

            itemno = "";
            FindViewById<TextView>(Resource.Id.tvItemDesc).Text = "";
            FindViewById<TextView>(Resource.Id.tvItemDesc2).Text = "";
            BoxTypes = new List<string>() { "Inner", "Outer" };
            FindViewById<Spinner>(Resource.Id.spnBoxType).Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, BoxTypes);
            FindViewById<Spinner>(Resource.Id.spnBoxType).ItemSelected += CountBinItemNo_ItemSelected;

            FindViewById<EditText>(Resource.Id.etBin).RequestFocus();
            FindViewById<EditText>(Resource.Id.etBin).KeyPress += OnBin_KeyPress;
            FindViewById<TextView>(Resource.Id.tvItem).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvItemDesc).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvItemDesc2).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvBoxType).Visibility = ViewStates.Gone;
            FindViewById<Spinner>(Resource.Id.spnBoxType).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvBinQuantity).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvBoxQuantity).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etBoxQuantity).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etQuantityPerInner).KeyPress += OnQtyPerInner_KeyPress;
            FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etQuantityPerOuter).KeyPress += OnQtyPerOuter_KeyPress;
            FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Click += (sender, e) =>
            {
                if (newDate != default(DateTime))
                {
                    today = newDate;
                }
                else
                {
                    today = DateTime.Today;
                }
                DatePickerDialog dialog = new DatePickerDialog(this, OnDateSet, today.Year, today.Month - 1, today.Day);
                dialog.DatePicker.MinDate = DateTime.Today.Millisecond;
                dialog.Show();
            };
            FindViewById<Button>(Resource.Id.btnCount).Click += OnCountClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnQtyPerOuter_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                if (FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility == ViewStates.Gone)
                {
                    CountBin();
                }
                else
                {
                    InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
                    var currentFocus = Window.CurrentFocus;
                    if (currentFocus != null)
                    {
                        inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
                    }
                }
            }
            else
                e.Handled = false;
        }

        private void CountBinItemNo_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            switch (FindViewById<Spinner>(Resource.Id.spnBoxType).SelectedItemId)
            {
                case 0:
                    FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Gone;
                    FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Gone;
                    break;
                case 1:
                    FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Visible;
                    break;
            }
        }

        private void OnBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                GetItem();
            }
            else
                e.Handled = false;
        }

        private void OnQtyPerInner_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                switch (FindViewById<Spinner>(Resource.Id.spnBoxType).SelectedItemId)
                {
                    case 0:
                        e.Handled = true;
                        if (FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility == ViewStates.Gone)
                        {
                            CountBin();
                        }
                        else
                        {
                            InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
                            var currentFocus = Window.CurrentFocus;
                            if (currentFocus != null)
                            {
                                inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
                            }
                        }
                        break;
                    case 1:
                        break;
                }
            }
            else
                e.Handled = false;
        }

        private void OnCountClick(object sender, EventArgs e)
        {
            if (FindViewById<TextView>(Resource.Id.tvItemDesc).Text == "")
            {
                GetItem();
            }
            else
            {
                CountBin();
            }
        }

        private void GetItem()
        {
            try
            {
                if (Helper.Demo)
                {
                    itemno = "1010";
                    FindViewById<TextView>(Resource.Id.tvItemDesc).Text = "Demo cykel";
                    FindViewById<TextView>(Resource.Id.tvItemDesc2).Text = "Herrer";
                    FindViewById<TextView>(Resource.Id.tvBinQuantity).Text = "Quantity: " + "26";

                    FindViewById<TextView>(Resource.Id.tvItem).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvItemDesc).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvItemDesc2).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvBoxType).Visibility = ViewStates.Visible;
                    FindViewById<Spinner>(Resource.Id.spnBoxType).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvBinQuantity).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvBoxQuantity).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etBoxQuantity).Visibility = ViewStates.Visible;
                    switch (FindViewById<Spinner>(Resource.Id.spnBoxType).SelectedItemId)
                    {
                        case 0:
                            FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Visible;
                            FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Gone;
                            FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Gone;
                            break;
                        case 1:
                            FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Visible;
                            FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Visible;
                            break;
                    }
                    FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etBoxQuantity).RequestFocus();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    item_root ResponseRoot = new item_root();

                    ws.GetItemInformation(ref ResponseRoot, FindViewById<EditText>(Resource.Id.etBin).Text);

                    item_response XmlResponse = ResponseRoot.item_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        item item = XmlResponse.item[0];
                        bin_line binLine = XmlResponse.bin_line[0];
                        item_tracking itemTracking = XmlResponse.item_tracking[0];
                        itemno = item.no;
                        FindViewById<TextView>(Resource.Id.tvItemDesc).Text = item.desc;
                        FindViewById<TextView>(Resource.Id.tvItemDesc2).Text = item.desc2;
                        FindViewById<TextView>(Resource.Id.tvBinQuantity).Text = "Quantity: " + binLine.qty;

                        FindViewById<TextView>(Resource.Id.tvItem).Visibility = ViewStates.Visible;
                        FindViewById<TextView>(Resource.Id.tvItemDesc).Visibility = ViewStates.Visible;
                        FindViewById<TextView>(Resource.Id.tvItemDesc2).Visibility = ViewStates.Visible;
                        FindViewById<Spinner>(Resource.Id.spnBoxType).Visibility = ViewStates.Visible;
                        FindViewById<TextView>(Resource.Id.tvBinQuantity).Visibility = ViewStates.Visible;
                        FindViewById<TextView>(Resource.Id.tvBoxQuantity).Visibility = ViewStates.Visible;
                        FindViewById<EditText>(Resource.Id.etBoxQuantity).Visibility = ViewStates.Visible;
                        switch (FindViewById<Spinner>(Resource.Id.spnBoxType).SelectedItemId)
                        {
                            case 0:
                                FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Visible;
                                FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Visible;
                                FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Gone;
                                FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Gone;
                                break;
                            case 1:
                                FindViewById<TextView>(Resource.Id.tvQuantityPerInner).Visibility = ViewStates.Visible;
                                FindViewById<EditText>(Resource.Id.etQuantityPerInner).Visibility = ViewStates.Visible;
                                FindViewById<TextView>(Resource.Id.tvQuantityPerOuter).Visibility = ViewStates.Visible;
                                FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Visibility = ViewStates.Visible;
                                break;
                        }
                        if (Convert.ToBoolean(itemTracking.ExpirationDate))
                        {
                            FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Visible;
                        }
                        FindViewById<EditText>(Resource.Id.etBoxQuantity).RequestFocus();

                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void CountBin()
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.ShowToast(this, $"Bin {FindViewById<EditText>(Resource.Id.etBin).Text} counted", false);
                    LoadView();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root navResponse = new default_root();

                    switch (FindViewById<Spinner>(Resource.Id.spnBoxType).SelectedItemId)
                    {
                        case 0:
                            ws.CountBinItemNo(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etBin).Text, itemno, Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etBoxQuantity).Text)), Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etQuantityPerInner).Text)), "0", Helper.FormatDate(newDate), 0);
                            break;
                        case 1:
                            ws.CountBinItemNo(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etBin).Text, itemno, Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etBoxQuantity).Text)), Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etQuantityPerInner).Text)), Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etQuantityPerOuter).Text)), Helper.FormatDate(newDate), 1);
                            break;
                    }

                    default_response XmlResponse = navResponse.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.ShowToast(this, XmlResponse.status_text, false);
                        LoadView();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            newDate = e.Date;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Text = newDate.ToShortDateString();
            InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
            var currentFocus = Window.CurrentFocus;
            if (currentFocus != null)
            {
                inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }
    }
}