﻿using System;
using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PickPushItem : Activity
    {
        private int UpdateSalesOrder = 0;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            LoadView();            
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.PickItem);

            FindViewById<EditText>(Resource.Id.etLocation).Text = "";
            FindViewById<EditText>(Resource.Id.etQty).Text = "";

            FindViewById<Button>(Resource.Id.btnPrintContent).Click += OnPrintContentClick;
            FindViewById<Button>(Resource.Id.btnDelete).Click += OnDeleteClick;
            FindViewById<TextView>(Resource.Id.tvCustomerNo).Text = Helper.CurrentCustomerNo;
            FindViewById<TextView>(Resource.Id.tvCustomerName).Text = Helper.CurrentCustomerName;
            FindViewById<TextView>(Resource.Id.tvPalletID).Text = Helper.CurrentPallet;
            FindViewById<EditText>(Resource.Id.etLocation).KeyPress += OnLocationKeyPress;
            FindViewById<EditText>(Resource.Id.etLocation).RequestFocus();
            FindViewById<EditText>(Resource.Id.etQty).KeyPress += OnQtyKeyPress;

            if (OrderClass.Lines.ContainsKey(Helper.CurrentLineNo))
            {
                FindViewById<TextView>(Resource.Id.tvItemNo).Text = OrderClass.Lines[Helper.CurrentLineNo].ItemNo;
                FindViewById<TextView>(Resource.Id.tvDesc).Text = OrderClass.Lines[Helper.CurrentLineNo].Description;
                FindViewById<TextView>(Resource.Id.tvInOutFormat).Text = OrderClass.Lines[Helper.CurrentLineNo].InOutFormat;
                FindViewById<TextView>(Resource.Id.tvLocation).Text = OrderClass.Lines[Helper.CurrentLineNo].PickBin;
                FindViewById<TextView>(Resource.Id.tvQty).Text = OrderClass.Lines[Helper.CurrentLineNo].InnerOuterQty + " " + OrderClass.Lines[Helper.CurrentLineNo].InnerOuter;
                switch (OrderClass.Lines[Helper.CurrentLineNo].InnerOuter)
                {
                    case "Outer":
                        FindViewById<TextView>(Resource.Id.tvQty).SetTextColor(Color.ParseColor("#01bfbf"));
                        break;
                    case "Inner":
                        FindViewById<TextView>(Resource.Id.tvQty).SetTextColor(Color.Red);
                        break;
                    default:
                        FindViewById<TextView>(Resource.Id.tvQty).SetTextColor(Color.Gold);
                        break;
                }
            }
            else
            {
                Finish();
            }
        }

        private void OnLocationKeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                InputMethodManager inputMethodManager = this.FindViewById<EditText>(Resource.Id.etQty).Context.GetSystemService(Android.Content.Context.InputMethodService) as InputMethodManager;
                inputMethodManager.ShowSoftInput(this.FindViewById<EditText>(Resource.Id.etQty), ShowFlags.Forced);
                inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);

                e.Handled = true;
            }
            else
                e.Handled = false;
        }

        private void OnQtyKeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                PickToPallet();
                e.Handled = true;
            }
            else
                e.Handled = false;
        }

        private void OnDeleteClick(object sender, EventArgs e)
        {
            var options = new[] { "OK", "Cancel" };
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.SetTitle("Delete line?");
            builder.SetItems(options, ((innersender, innerargs) =>
            {
                switch (innerargs.Which)
                {
                    case 0:
                        {
                            try
                            {
                                ScannerInterface ws = Helper.GetInterface(this);

                                default_root ResponseRoot = new default_root();

                                ws.UpdateSOLines(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentLineNo);

                                default_response XmlResponse = ResponseRoot.default_response[0];

                                if (Helper.IsOK(XmlResponse.status))
                                {
                                    OrderClass.Lines.Remove(Helper.CurrentLineNo);
                                    Helper.CurrentLineNo += 10000;
                                    LoadView();                                    
                                }
                                else
                                {
                                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                                }
                            }
                            catch (Exception ex)
                            {
                                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                            }
                            break;
                        }
                    case 1:
                        {
                            break;
                        }
                }
            }));
            builder.Show();
        }

        private void OnPrintContentClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(PickPushOrder));
            intent.PutExtra("PrintContent", true);
            SetResult(Result.Ok, intent);
            Finish();
        }

        private void PickToPallet()
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root ResponseRoot = new default_root();

                ws.PickToPallet(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentLineNo, Helper.CurrentPallet, FindViewById<EditText>(Resource.Id.etLocation).Text, Convert.ToInt32(FindViewById<EditText>(Resource.Id.etQty).Text), UpdateSalesOrder);

                default_response XmlResponse = ResponseRoot.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    if (OrderClass.Lines[Helper.CurrentLineNo].InnerOuterQty > Convert.ToInt32(FindViewById<EditText>(Resource.Id.etQty).Text) & (UpdateSalesOrder == 2))
                    {
                        Finish();
                    }
                    else
                    {
                        OrderClass.Lines.Remove(Helper.CurrentLineNo);

                        if (OrderClass.Lines.Count == 0)
                        {
                            FindViewById<Button>(Resource.Id.btnPrintContent).CallOnClick();
                        }
                        Helper.CurrentLineNo += 10000;

                        UpdateSalesOrder = 0;
                        LoadView();
                    }
                }
                else if (Helper.IsNeddInfo(XmlResponse.status))
                {
                    var options = new[] { "Update", "Continue" };
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.SetTitle("Do you want to update the quantity on the sales order?");
                    builder.SetItems(options, ((innersender, innerargs) =>
                    {
                        switch (innerargs.Which)
                        {
                            case 0:
                                {
                                    UpdateSalesOrder = 1;
                                    PickToPallet();
                                    break;
                                }
                            case 1:
                                {
                                    UpdateSalesOrder = 2;
                                    PickToPallet();
                                    break;
                                }
                        }
                    }));
                    builder.Show();
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}