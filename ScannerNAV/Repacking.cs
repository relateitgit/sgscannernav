﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "Create Pallet", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Repacking : Activity
    {
        private DateTime newDate;
        private DateTime today;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            LoadView();
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.Repacking);

            FindViewById<EditText>(Resource.Id.etPalletNo).Text = "";
            FindViewById<EditText>(Resource.Id.etNewPalletNo).Text = "";
            FindViewById<EditText>(Resource.Id.etContentNo).Text = "";
            FindViewById<EditText>(Resource.Id.etQuantity).Text = "";
            FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Gone;

            FindViewById<EditText>(Resource.Id.etPalletNo).RequestFocus();
            FindViewById<EditText>(Resource.Id.etQuantity).KeyPress += OnQantity_KeyPress;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Click += (sender, e) =>
            {
                if (newDate != default(DateTime))
                {
                    today = newDate;
                }
                else
                {
                    today = DateTime.Today;
                }
                DatePickerDialog dialog = new DatePickerDialog(this, OnDateSet, today.Year, today.Month - 1, today.Day);
                dialog.DatePicker.MinDate = DateTime.Today.Millisecond;
                dialog.Show();
            };
            FindViewById<Button>(Resource.Id.btnCreate).Click += OnCreateClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnQantity_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                if (FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility == ViewStates.Gone)
                {
                    FindViewById<Button>(Resource.Id.btnCreate).PerformClick();
                }
                else
                {
                    InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
                    var currentFocus = Window.CurrentFocus;
                    if (currentFocus != null)
                    {
                        inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
                    }
                }
            }
            else
                e.Handled = false;
        }

        private void OnCreateClick(object sender, EventArgs e)
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.ShowToast(this, $"Pallet Content moved from {FindViewById<EditText>(Resource.Id.etPalletNo).Text} to new pallet {FindViewById<EditText>(Resource.Id.etNewPalletNo).Text} created", false);
                    LoadView();
                }
                else
                {
                    default_root navResponse = new default_root();
                    ScannerInterface ws = Helper.GetInterface(this);
                    ws.Repacking(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etPalletNo).Text, FindViewById<EditText>(Resource.Id.etNewPalletNo).Text, FindViewById<EditText>(Resource.Id.etContentNo).Text, Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etQuantity).Text)), Helper.FormatDate(newDate));
                    default_response XmlResponse = navResponse.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.ShowToast(this, XmlResponse.status_text, false);
                        LoadView();
                    }
                    else if (Helper.IsNeddInfo(XmlResponse.status))
                    {
                        FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Visible;
                        FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            newDate = e.Date;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Text = newDate.ToShortDateString();
            InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
            var currentFocus = Window.CurrentFocus;
            if (currentFocus != null)
            {
                inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }
    }
}