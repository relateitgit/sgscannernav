﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MenuCount : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.MenuCount);
            
            FindViewById<Button>(Resource.Id.btnCountPallet).Click += OnCountPallet_Click;            
            FindViewById<Button>(Resource.Id.btnCountBinBarcode).Click += OnCountBinBarcode_Click;
            FindViewById<Button>(Resource.Id.btnCountBinItemNo).Click += OnCountBinItem_Click;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();

            // Add version to the page
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);

            if (Helper.Demo)
            {
                FindViewById<Button>(Resource.Id.btnCountPallet).Visibility = ViewStates.Invisible;
            }
        }

        private void OnCountPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(CountPallet));
        }

        private void OnCountBinBarcode_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(CountBinBarcode));
        }

        private void OnCountBinItem_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(CountBinItemNo));
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}