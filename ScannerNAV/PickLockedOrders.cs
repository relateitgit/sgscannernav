﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "PickLockedOrders", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PickLockedOrders : Activity
    {
        //private Button ButtonBack;
        
        private ListView ListViewLockedOrders;
        private List<string> itemlist = new List<string>();
        private string ItemValue;

        private bool DataChange; //Is true if lines have been unlocked



        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Show this Screen
            SetContentView(Resource.Layout.PickLockedOrders);
            
            //ButtonBack = FindViewById<Button>(Resource.Id.buttonBack);
            //ButtonBack.Click += OnButtonBack_Click;
                        
            ListViewLockedOrders = FindViewById<ListView>(Resource.Id.listViewLockedOrders);
            ListViewLockedOrders.ItemClick += OnListViewLockedOrders_ItemClick; // ListView Item Click Listener



            FillItemList();
            UpdateLockedOrdersList();


        }

        private void FillItemList()
        {

            // Defined Array values to show in ListView
            String[] values = new String[] { "Android List View",
                                             "Adapter implementation",
                                             "Simple List View In Android",
                                             "Create List View Android",
                                             "Android Example",
                                             "List View Source Code",
                                             "List View Array Adapter",
                                             "Android Example List View",
                                             "https://androidexample.com/Create_A_Simple_Listview_-_Android_Example/index.php?view=article_discription&aid=65",
                                             "Order 1",
                                             "Order 2",
                                             "Order 3",
                                             "Order 4",
                                             "https://www.c-sharpcorner.com/article/xamarin-android-create-android-simple-list-view-app/"
                                            };

            itemlist.AddRange(values);
            itemlist.Add("Sidste linie");

        }


        private void UpdateLockedOrdersList()
        {
            ArrayAdapter<String> adapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, itemlist);

            // Assign adapter to ListView
            ListViewLockedOrders.Adapter = adapter;
        }

        
        public void OnListViewLockedOrders_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            ItemValue = (String)ListViewLockedOrders.GetItemAtPosition(e.Position);
            
            Toast.MakeText(this,ItemValue,ToastLength.Long).Show();


            using (var builder = new AlertDialog.Builder(this))
            {
                var title = "Unlock: " + ItemValue + " ?";
                builder.SetTitle(title);
                builder.SetPositiveButton("Yes", AlertDialogOkAction);
                builder.SetNegativeButton("Nope", AlertDialogCancelAction);
                var myCustomDialog = builder.Create();

                myCustomDialog.Show();
            }
        }

        private void AlertDialogOkAction(object sender, DialogClickEventArgs e)
        {
            itemlist.Remove(ItemValue);

            UpdateLockedOrdersList();
            DataChange = true;
        }
        private void AlertDialogCancelAction(object sender, DialogClickEventArgs e)
        {
            //Do nothing
        }

        //public void OnButtonBack_Click(object sender, EventArgs e)
        //{
        //    if (DataChange)
        //    {
        //        SetResult(Result.Ok);
        //    }
        //    Finish();
        //}
    }
}