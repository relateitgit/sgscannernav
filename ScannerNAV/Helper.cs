﻿using Android.App;
using Android.Content;
using ScannerNAV.Webservice;
using Android.Widget;
using Android.Graphics;
using Android.Views;
using System.Collections.Generic;
using System;

namespace ScannerNAV
{
    public class Helper
    {
        internal static void ShowAlertDialog(Context Context, string Title, string Message)
        {
            AlertDialog.Builder dialog = new AlertDialog.Builder(Context);
            AlertDialog alert = dialog.Create();
            alert.SetTitle(Title);
            alert.SetMessage(Message);
            alert.SetButton("OK", (c, ev) => { });
            alert.Show();
        }

        internal static void ShowToast(Context Context, string status_text, bool warning)
        {
            Toast toast = Toast.MakeText(Context, status_text, ToastLength.Long);
            if (warning)
            { toast.View.SetBackgroundColor(Color.Yellow); }
            else
            { toast.View.SetBackgroundColor(Color.Green); }
            TextView view = (TextView)toast.View.FindViewById(Android.Resource.Id.Message);
            view.SetTextColor(Color.Black);
            toast.Show();
        }

        internal static string FormatDate(DateTime date)
        {
            if (date != default(DateTime))
            {
                return date.Day + "," + date.Month + "," + date.Year;
            }
            else
            {
                return "";
            }
        }

        internal static string FormatDecimal(decimal input)
        {
            return input.ToString("#");
        }

        internal static decimal FormatTextDecimal(string input)
        {
            if (input != "")
            {
                return Convert.ToDecimal(input) / 100;
            }
            else
            {
                return 0;
            }
        }

        internal static ScannerInterface GetInterface(Context Context)
        {
            ScannerInterface ws = new ScannerInterface
            {
                Credentials = new System.Net.NetworkCredential(WSUser, WSPassword),
                Url = WSUrl,
                Timeout = 300000
            };
            return ws;
        }

        internal static string GetStatus()
        {
            var Items = new List<string>() { "Production", "Test", "Custom", "Demo" };
            return Items[Convert.ToInt32(Helper.MySystem)] + " | User: " + RescourceNo;
        }

        internal static string GetVersion(Context Context)
        {
            return "Version: "
                + Context.PackageManager.GetPackageInfo(Context.PackageName, 0).VersionName
                + Context.PackageManager.GetPackageInfo(Context.PackageName, 0).VersionCode;
        }

        internal static bool IsOK(string Value)
        {
            if (Value.ToLower() == "ok")
                return true;
            else
                return false;
        }

        internal static bool IsNeddInfo(string Value)
        {
            if (Value == "needInfo")
                return true;
            else
                return false;
        }

        internal static string WSUser
        {
            get { return GetKey("WSUser"); }
            set { SetKey("WSUser", value); }
        }

        internal static string WSPassword
        {
            get { return GetKey("WSPassword"); }
            set { SetKey("WSPassword", value); }
        }

        internal static string WSUrl
        {
            get { return GetKey("WSUrl"); }
            set { SetKey("WSUrl", value); }
        }

        internal static string RescourceNo
        {
            get { return GetKey("RescourceNo"); }
            set { SetKey("RescourceNo", value); }
        }

        internal static string Zone
        {
            get { return GetKey("Zone"); }
            set { SetKey("Zone", value); }
        }

        internal static bool CheckInput
        {
            get { return System.Convert.ToBoolean(GetKey("CheckInput")); }
            set { SetKey("CheckInput", value.ToString()); }
        }

        internal static bool CloseActivity
        {
            get { return System.Convert.ToBoolean(GetKey("CloseActivity")); }
            set { SetKey("CloseActivity", value.ToString()); }
        }

        internal static string MySystem
        {
            get { return GetKey("System"); }
            set
            {
                SetKey("System", value);
                switch (value)
                {
                    case "0":
                        {
                            Demo = false;
                            WSUser = "Grenes\\Scanner.user";
                            WSPassword = "Ytrewq321";
                            WSUrl = "http://10.10.12.35:7447/Scanner/WS/SGI/Codeunit/ScannerInterface";
                            break;
                        }
                    case "1":
                        {
                            Demo = false;
                            WSUser = "Grenes\\Scanner.user";
                            WSPassword = "Ytrewq321";
                            WSUrl = "http://10.10.15.31:8247/Test2/WS/SGI/Codeunit/ScannerInterface";
                            break;
                        }
                    case "2":
                        {
                            Demo = false;
                            break;
                        }
                    case "3":
                        {
                            Demo = true;
                            break;
                        }
                }
            }
        }

        internal static bool Demo
        {
            get { return System.Convert.ToBoolean(GetKey("Demo")); }
            set { SetKey("Demo", value.ToString()); }
        }

        internal static string CurrentBin
        {
            get { return GetKey("CurrentBin"); }
            set { SetKey("CurrentBin", value); }
        }

        internal static string CurrentPallet
        {
            get { return GetKey("CurrentPallet"); }
            set { SetKey("CurrentPallet", value); }
        }

        internal static bool CurrentPalletFree
        {
            get { return System.Convert.ToBoolean(GetKey("CurrentPalletFree")); }
            set { SetKey("CurrentPalletFree", value.ToString()); }
        }

        internal static string CurrentCustomerNo
        {
            get { return GetKey("CurrentCustomerNo"); }
            set { SetKey("CurrentCustomerNo", value); }
        }

        internal static string CurrentCustomerName
        {
            get { return GetKey("CurrentCustomerName"); }
            set { SetKey("CurrentCustomerName", value); }
        }

        internal static int CurrentLineNo
        {
            get { return System.Convert.ToInt32(GetKey("CurrentLineNo")); }
            set { SetKey("CurrentLineNo", value.ToString()); }
        }

        private static void SetKey(string KeyName, string KeyValue)
        {
            var prefs = Application.Context.GetSharedPreferences("ScannerNAV", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString(KeyName, KeyValue);
            prefEditor.Commit();
        }

        private static string GetKey(string KeyName)
        {
            //retreive 
            var prefs = Application.Context.GetSharedPreferences("ScannerNAV", FileCreationMode.Private);
            return prefs.GetString(KeyName, null);
        }
    }

    public class AdapterActivityLog : BaseAdapter<ActivityLog>
    {
        List<ActivityLog> items;
        Activity context;
        public AdapterActivityLog(Activity context, List<ActivityLog> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override ActivityLog this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, null);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.CreateDate + " " + item.CreateTime;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.PalletCode;
            view.FindViewById<TextView>(Resource.Id.textView3).Text = item.Action;

            if (item.ShowColor)
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Color.ParseColor("#007575"));
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
            return view;
        }
    }

    public class AdapterBin : BaseAdapter<Bin>
    {
        List<Bin> items;
        Activity context;
        public AdapterBin(Activity context, List<Bin> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Bin this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, null);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.Code;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.Quantity.ToString();
            view.FindViewById<TextView>(Resource.Id.textView3).Text = item.Store;

            if (item.ShowColor)
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Color.ParseColor("#007575"));
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
            return view;
        }
    }

    public class AdapterItemLocation : BaseAdapter<ItemLocation>
    {
        List<ItemLocation> items;
        Activity context;
        public AdapterItemLocation(Activity context, List<ItemLocation> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override ItemLocation this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, null);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.Bin + " " + item.Type;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.ItemNo + " " + item.Description;
            view.FindViewById<TextView>(Resource.Id.textView3).Text = item.Quantity + " " + item.QuantityType;

            if (item.ShowColor)
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Color.ParseColor("#007575"));
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
            return view;
        }
    }

    public class AdapterOrderOrders : BaseAdapter<Order>
    {
        List<Order> items;
        Activity context;
        public AdapterOrderOrders(Activity context, List<Order> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Order this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, null);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.CustomerNo;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.CustomerName;
            view.FindViewById<TextView>(Resource.Id.textView3).Visibility = ViewStates.Gone;
            //view.FindViewById<ImageView>(Resource.Id.imageView1).SetBackgroundColor(item.Color);
            if (item.ShowColor)
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Color.ParseColor("#007575"));
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Android.Graphics.Color.Transparent);
            }

            return view;
        }
    }

    public class AdapterOrderLines : BaseAdapter<OrderLine>
    {
        List<OrderLine> items;
        Activity context;
        public AdapterOrderLines(Activity context, List<OrderLine> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override OrderLine this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, null);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.PickBin;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.Description;
            view.FindViewById<TextView>(Resource.Id.textView3).Text = item.InnerOuterQty + " " + item.InnerOuter;
            //view.FindViewById<ImageView>(Resource.Id.imageView1).SetBackgroundColor(item.Color);
            if (item.ShowColor)
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Color.ParseColor("#007575"));
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
            return view;
        }
    }

    public class AdapterStorePallet : BaseAdapter<StorePallet>
    {
        List<StorePallet> items;
        Activity context;
        public AdapterStorePallet(Activity context, List<StorePallet> items) : base()
        {
            this.context = context;
            this.items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override StorePallet this[int position]
        {
            get { return items[position]; }
        }

        public override int Count
        {
            get { return items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var item = items[position];

            View view = convertView;
            if (view == null) // no view to re-use, create new
                view = context.LayoutInflater.Inflate(Resource.Layout.ListItem, null);
            view.FindViewById<TextView>(Resource.Id.textView1).Text = item.Id;
            view.FindViewById<TextView>(Resource.Id.textView2).Text = item.Status;
            if (item.FreePallet)
            {
                view.FindViewById<TextView>(Resource.Id.textView3).Text = item.Bin + " | Free Pallet";
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView3).Text = item.Bin;
            }

            if (item.ShowColor)
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Color.ParseColor("#007575"));
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Color.ParseColor("#007575"));
            }
            else
            {
                view.FindViewById<TextView>(Resource.Id.textView1).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView2).SetBackgroundColor(Android.Graphics.Color.Transparent);
                view.FindViewById<TextView>(Resource.Id.textView3).SetBackgroundColor(Android.Graphics.Color.Transparent);
            }
            return view;
        }
    }  

    public class ActivityLog
    {
        public string EntryNo { get; set; }
        public string CreateDate { get; set; }
        public string CreateTime { get; set; }
        public string UserID { get; set; }
        public string PalletCode { get; set; }
        public string PalletLine { get; set; }
        public string SalesOrder { get; set; }
        public string SalesOrderLine { get; set; }
        public string Store { get; set; }
        public string Action { get; set; }
        public Android.Graphics.Color Color { get; set; }
        public Android.Graphics.Bitmap Bitmap { get; set; }
        public bool ShowColor { get; set; }
    }

    public class Bin
    {
        public string Code { get; set; }
        public string Store { get; set; }
        public decimal Quantity { get; set; }
        public Android.Graphics.Color Color { get; set; }
        public Android.Graphics.Bitmap Bitmap { get; set; }
        public bool ShowColor { get; set; }
    }

    public class ItemLocation
    {
        public string Zone { get; set; }
        public string Bin { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public string QuantityType { get; set; }
        public string Type { get; set; }
        public Android.Graphics.Color Color { get; set; }
        public Android.Graphics.Bitmap Bitmap { get; set; }
        public bool ShowColor { get; set; }
    }
    
    public class Order
    {
        public string CustomerNo { get; set; }
        public string WhoisPicking { get; set; }
        public string CustomerName { get; set; }
        public Android.Graphics.Color Color { get; set; }
        public Android.Graphics.Bitmap Bitmap { get; set; }
        public bool ShowColor { get; set; }
    }

    public class OrderLine
    {
        public int LineNo { get; set; }
        public string ItemNo { get; set; }
        public string Description { get; set; }
        public string PickBin { get; set; }
        public string InnerOuter { get; set; }
        public decimal Quantity { get; set; }
        public decimal InnerOuterQty { get; set; }
        public string InOutFormat { get; set; }
        public Android.Graphics.Color Color { get; set; }
        public bool ShowColor { get; set; }
    }
    public class StorePallet
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string Zone { get; set; }
        public string Bin { get; set; }
        public string StoreName { get; set; }
        public bool FreePallet { get; set; }
        public Android.Graphics.Color Color { get; set; }
        public Android.Graphics.Bitmap Bitmap { get; set; }
        public bool ShowColor { get; set; }
    }   

    public static class OrderClass
    {
        public static Dictionary<string, Order> Orders = new Dictionary<string, Order>();

        public static Dictionary<int, OrderLine> Lines = new Dictionary<int, OrderLine>();
    }

    public static class ObjectTypeHelper
    {
        //https://forums.xamarin.com/discussion/14863/cannot-cast-single-custom-listview-row-to-its-lists-type

        public static T Cast<T>(this Java.Lang.Object obj) where T : class
        {
            var propertyInfo = obj.GetType().GetProperty("Instance");
            return propertyInfo == null ? null : propertyInfo.GetValue(obj, null) as T;
        }
    }

}