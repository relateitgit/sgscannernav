﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PrintPallet : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here            
            LoadView();
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.PrintPallet);

            FindViewById<EditText>(Resource.Id.etPalletNo).Text = "";

            FindViewById<EditText>(Resource.Id.etPalletNo).KeyPress += OnPallet_KeyPress;
            FindViewById<EditText>(Resource.Id.etPalletNo).RequestFocus();
            FindViewById<Button>(Resource.Id.btnPrint).Click += OnCreateClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnPallet_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnPrint).PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnCreateClick(object sender, EventArgs e)
        {
            try
            {
                default_root navResponse = new default_root();
                ScannerInterface ws = Helper.GetInterface(this);
                ws.PrintContent(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etPalletNo).Text);
                default_response XmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    Helper.ShowToast(this, XmlResponse.status_text, false);
                    LoadView();
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }
    }
}