﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "CountPallet")]
    public class CountBinBarcode : Activity
    {
        private DateTime newDate;
        private DateTime today;
        private bool ItemExpirationDate;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            LoadView();
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.CountBinBarcode);

            FindViewById<EditText>(Resource.Id.etBin).Text = "";
            FindViewById<EditText>(Resource.Id.etContentNo).Text = "";
            FindViewById<TextView>(Resource.Id.tvBinQuantity).Text = "";
            FindViewById<EditText>(Resource.Id.etQuantity).Text = "";

            FindViewById<TextView>(Resource.Id.tvBinQuantity).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvQuantity).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etQuantity).Visibility = ViewStates.Gone;
            FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Gone;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Gone;

            FindViewById<EditText>(Resource.Id.etBin).RequestFocus();
            FindViewById<EditText>(Resource.Id.etContentNo).KeyPress += OnContent_KeyPress;
            FindViewById<EditText>(Resource.Id.etQuantity).KeyPress += OnQuantity_KeyPress;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Click += (sender, e) =>
            {
                if (newDate != default(DateTime))
                {
                    today = newDate;
                }
                else
                {
                    today = DateTime.Today;
                }
                DatePickerDialog dialog = new DatePickerDialog(this, OnDateSet, today.Year, today.Month - 1, today.Day);
                dialog.DatePicker.MinDate = DateTime.Today.Millisecond;
                dialog.Show();
            };
            FindViewById<Button>(Resource.Id.btnCount).Click += OnCountClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnContent_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                GetItem();
            }
            else
                e.Handled = false;
        }

        private void OnQuantity_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                if (FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility == ViewStates.Gone)
                {
                    FindViewById<Button>(Resource.Id.btnCount).PerformClick();
                }
                else
                {
                    InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
                    var currentFocus = Window.CurrentFocus;
                    if (currentFocus != null)
                    {
                        inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
                    }
                }
            }
            else
                e.Handled = false;
        }

        private void OnCountClick(object sender, EventArgs e)
        {
            if (FindViewById<TextView>(Resource.Id.tvBinQuantity).Text == "")
            {
                GetItem();
            }
            else
            {
                CountBin();
            }
        }

        private void GetItem()
        {
            try
            {
                if (Helper.Demo)
                {
                    FindViewById<TextView>(Resource.Id.tvBinQuantity).Text = "Quantity: " + "26";
                    FindViewById<TextView>(Resource.Id.tvBinQuantity).Visibility = ViewStates.Visible;
                    FindViewById<TextView>(Resource.Id.tvQuantity).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etQuantity).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etQuantity).RequestFocus();
                    FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Visible;
                    FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Visible;
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    item_root ResponseRoot = new item_root();

                    ws.GetItemInformationBarcode(ref ResponseRoot, FindViewById<EditText>(Resource.Id.etBin).Text, FindViewById<TextView>(Resource.Id.etContentNo).Text);

                    item_response XmlResponse = ResponseRoot.item_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        item item = XmlResponse.item[0];
                        bin_line binLine = XmlResponse.bin_line[0];
                        item_tracking itemtracking = XmlResponse.item_tracking[0];
                        FindViewById<TextView>(Resource.Id.tvBinQuantity).Text = "Quantity: " + binLine.qty;
                        FindViewById<TextView>(Resource.Id.tvBinQuantity).Visibility = ViewStates.Visible;
                        FindViewById<TextView>(Resource.Id.tvQuantity).Visibility = ViewStates.Visible;
                        FindViewById<EditText>(Resource.Id.etQuantity).Visibility = ViewStates.Visible;
                        FindViewById<EditText>(Resource.Id.etQuantity).RequestFocus();
                        Boolean.TryParse(itemtracking.ExpirationDate, out ItemExpirationDate);
                        if (ItemExpirationDate)
                        {
                            FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Visible;
                        } else
                        {
                            FindViewById<TextView>(Resource.Id.tvExpirationDate).Visibility = ViewStates.Gone;
                            FindViewById<EditText>(Resource.Id.etExpirationDate).Visibility = ViewStates.Gone;
                        }                        
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void CountBin()
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.ShowToast(this, $"Bin {FindViewById<EditText>(Resource.Id.etBin).Text} counted", false);
                    LoadView();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root navResponse = new default_root();

                    ws.CountBinBarcode(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etBin).Text, FindViewById<EditText>(Resource.Id.etContentNo).Text, Helper.FormatDecimal(Convert.ToDecimal(FindViewById<EditText>(Resource.Id.etQuantity).Text)), Helper.FormatDate(newDate));

                    default_response XmlResponse = navResponse.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.ShowToast(this, XmlResponse.status_text, false);
                        LoadView();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            newDate = e.Date;
            FindViewById<EditText>(Resource.Id.etExpirationDate).Text = newDate.ToShortDateString();
            InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
            var currentFocus = Window.CurrentFocus;
            if (currentFocus != null)
            {
                inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }
    }
}