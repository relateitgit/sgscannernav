﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "CountPallet")]
    public class FindItemLocation : Activity
    {
        private List<ItemLocation> OrderItems = new List<ItemLocation>();
        private bool ShowList = false;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.FindItemLocation);
            
            FindViewById<EditText>(Resource.Id.etContentNo).KeyPress += OnContent_KeyPress;
            FindViewById<EditText>(Resource.Id.etItemNo).KeyPress += OnetItemNo_KeyPress;
            FindViewById<Button>(Resource.Id.btnFind).Click += OnFindClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
            LoadView();
        }

        private void LoadView()
        {
            FindViewById<EditText>(Resource.Id.etContentNo).Text = "";
            FindViewById<EditText>(Resource.Id.etItemNo).Text = "";

            if (ShowList)
            {
                FindViewById<TextView>(Resource.Id.tvContentNo).Visibility = ViewStates.Gone;
                FindViewById<EditText>(Resource.Id.etContentNo).Visibility = ViewStates.Gone;
                FindViewById<TextView>(Resource.Id.tvItemNo).Visibility = ViewStates.Gone;
                FindViewById<EditText>(Resource.Id.etItemNo).Visibility = ViewStates.Gone;
                FindViewById<Button>(Resource.Id.btnFind).Visibility = ViewStates.Gone;

                InputMethodManager inputManager = (InputMethodManager)GetSystemService(Context.InputMethodService);
                var currentFocus = Window.CurrentFocus;
                if (currentFocus != null)
                {
                    inputManager.HideSoftInputFromWindow(currentFocus.WindowToken, HideSoftInputFlags.None);
                }
            }
            else
            {
                OrderItems.Clear();
                FindViewById<TextView>(Resource.Id.tvContentNo).Visibility = ViewStates.Visible;
                FindViewById<EditText>(Resource.Id.etContentNo).Visibility = ViewStates.Visible;
                FindViewById<TextView>(Resource.Id.tvItemNo).Visibility = ViewStates.Visible;
                FindViewById<EditText>(Resource.Id.etItemNo).Visibility = ViewStates.Visible;
                FindViewById<Button>(Resource.Id.btnFind).Visibility = ViewStates.Visible;
                FindViewById<EditText>(Resource.Id.etContentNo).RequestFocus();

                InputMethodManager inputMethodManager = this.FindViewById<EditText>(Resource.Id.etContentNo).Context.GetSystemService(Android.Content.Context.InputMethodService) as InputMethodManager;
                inputMethodManager.ShowSoftInput(this.FindViewById<EditText>(Resource.Id.etContentNo), ShowFlags.Forced);
                inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
            }
        }

        private void OnContent_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnFind).CallOnClick();
            }
            else
                e.Handled = false;

            FindViewById<TextView>(Resource.Id.etItemNo).Text = "";
        }

        private void OnetItemNo_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnFind).CallOnClick();
            }
            else
                e.Handled = false;

            FindViewById<EditText>(Resource.Id.etContentNo).Text = "";
        }

        private void OnFindClick(object sender, EventArgs e)
        {
            try
            {
                if (Helper.Demo)
                {
                    OrderItems.Add(new ItemLocation
                    {
                        Zone = "STOCK1",
                        Bin = "112-3123-01",
                        ItemNo = "1010",
                        Description = "Demovare 1",
                        Quantity = Convert.ToDecimal(55),
                        QuantityType = "Outer",
                        Type = "Item",
                        ShowColor = true
                    });
                    OrderItems.Add(new ItemLocation
                    {
                        Zone = "STOCK2",
                        Bin = "212-2216-02",
                        ItemNo = "1010",
                        Description = "Demovare 1",
                        Quantity = Convert.ToDecimal(5),
                        QuantityType = "Outer",
                        Type = "Item",
                        ShowColor = false
                    });
                    OrderItems.Add(new ItemLocation
                    {
                        Zone = "STOCK2",
                        Bin = "201-0120-01",
                        ItemNo = "1010",
                        Description = "Demovare 1",
                        Quantity = Convert.ToDecimal(21),
                        QuantityType = "Outer",
                        Type = "Pallet",
                        ShowColor = true
                    });
                    FindViewById<ListView>(Resource.Id.listView).Adapter = new AdapterItemLocation(this, OrderItems);
                    ShowList = true;
                    LoadView();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    finditem_root navResponse = new finditem_root();

                    ws.GetFindItem(ref navResponse, FindViewById<EditText>(Resource.Id.etItemNo).Text, FindViewById<EditText>(Resource.Id.etContentNo).Text);

                    finditem_response XmlResponse = navResponse.finditem_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        bool LineColor = false;
                        OrderItems = new List<ItemLocation>(XmlResponse.finditem.Length);
                        foreach (finditem item in XmlResponse.finditem)
                        {
                            LineColor = !LineColor;
                            OrderItems.Add(new ItemLocation
                            {
                                Zone = item.zone,
                                Bin = item.bin,
                                ItemNo = item.itemno,
                                Description = item.itemdesc,
                                Quantity = Helper.FormatTextDecimal(item.quantity),
                                QuantityType = item.quantity_type,
                                Type = item.type,
                                ShowColor = LineColor
                            });
                        }
                        FindViewById<ListView>(Resource.Id.listView).Adapter = new AdapterItemLocation(this, OrderItems);
                        ShowList = true;
                        LoadView();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            if (ShowList)
            {
                ShowList = false;
                LoadView();
            }
            else
            {
                base.OnBackPressed();
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }
    }
}