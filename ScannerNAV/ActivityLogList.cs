﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class ActivityLogList : Activity
    {
        private ProgressBar pbProgress;
        private String ResponseTitle = "";
        private String ResponseMassage = "";
        private LinearLayout LLAll;
        private List<ActivityLog> logItems = new List<ActivityLog>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.ActivityLogList);

            pbProgress = FindViewById<ProgressBar>(Resource.Id.pbProgress);
            pbProgress.Visibility = ViewStates.Visible;
            LLAll = FindViewById<LinearLayout>(Resource.Id.LLAll);
            LLAll.Visibility = ViewStates.Gone;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {
            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                FindViewById<ListView>(Resource.Id.myListView).Adapter = new AdapterActivityLog(this, logItems);
            }
            else
            {
                Helper.ShowAlertDialog(this, ResponseTitle, ResponseMassage);
            }
            pbProgress.Visibility = ViewStates.Gone;
            LLAll.Visibility = ViewStates.Visible;
        }

        private Boolean LoadData()
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                activitylog_root ResponseRoot = new activitylog_root();

                ws.GetActivityLog(ref ResponseRoot, Helper.RescourceNo);

                activitylog_response XmlResponse = ResponseRoot.activitylog_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    bool LineColor = false;
                    logItems = new List<ActivityLog>(XmlResponse.activitylog.Length);
                    foreach (activitylog item in XmlResponse.activitylog)
                    {
                        LineColor = !LineColor;
                        logItems.Add(new ActivityLog
                        {
                            EntryNo = item.entry_no.ToString(),
                            CreateDate = item.create_date,
                            CreateTime = item.create_time,
                            UserID = item.user_id,
                            PalletCode = item.pallet_code,
                            PalletLine = item.pallet_line.ToString(),
                            SalesOrder = item.sales_order,
                            SalesOrderLine = item.sales_order_line.ToString(),
                            Store = item.store,
                            Action = item.action,
                            ShowColor = LineColor
                        });
                    }                    
                }
                else
                {
                    ResponseTitle = XmlResponse.status;
                    ResponseMassage = XmlResponse.status_text;
                    return false;
                }
            }
            catch (Exception ex)
            {
                ResponseTitle = "ERROR";
                ResponseMassage = ex.Message;
                return false;
            }
            return true;
        }

        override public void OnBackPressed()
        {
            Finish();
        }
    }
}