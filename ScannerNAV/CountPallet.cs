﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "CountPallet")]
    public class CountPallet : Activity
    {
        private Button btnCount;
        private EditText etPalletNo;
        private EditText etBin;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.CountPallet);

            etPalletNo = FindViewById<EditText>(Resource.Id.etPalletNo);
            etPalletNo.RequestFocus();

            etBin = FindViewById<EditText>(Resource.Id.etBin);
            etBin.KeyPress += OnBin_KeyPress;

            btnCount = FindViewById<Button>(Resource.Id.btnCount);
            btnCount.Click += OnCountClick;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                btnCount.PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnCountClick(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                ws.CountPallet(ref navResponse, Helper.RescourceNo, etPalletNo.Text, etBin.Text);

                default_response XmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    Helper.ShowToast(this, XmlResponse.status_text, false);
                    etPalletNo.Text = "";
                    etBin.Text = "";
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }
    }
}