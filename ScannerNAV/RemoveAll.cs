﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Remove Error Pallet", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class RemoveAll : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.RemoveAll);

            FindViewById<EditText>(Resource.Id.etBin).KeyPress += EmptyBin_KeyPress;
            FindViewById<EditText>(Resource.Id.etBin).RequestFocus();

            FindViewById<Button>(Resource.Id.btnEmptyBin).Click += OnEmptyClick;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void EmptyBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnEmptyBin).PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnEmptyClick(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                ws.RemoveAll(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etBin).Text);

                default_response XmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    Helper.ShowToast(this, XmlResponse.status_text, false);
                    Finish();
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}