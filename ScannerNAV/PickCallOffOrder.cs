﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PickCallOffOrder : Activity
    {
        private ProgressBar pbProgress;
        private String ResponseTitle = "";
        private String ResponseMassage = "";
        private LinearLayout LLAll;
        private Button btnPrintContent;
        private ListView OrderLines;
        private TextView tvCustomerNo;
        private TextView tvCustomerName;
        private TextView tvPalletID;
        private CheckBox cbPalletFree;
        private List<OrderLine> orderItems = new List<OrderLine>();
        private int PickLineNo;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.PickOrder);

            pbProgress = FindViewById<ProgressBar>(Resource.Id.pbProgress);
            pbProgress.Visibility = ViewStates.Visible;
            LLAll = FindViewById<LinearLayout>(Resource.Id.LLAll);
            LLAll.Visibility = ViewStates.Gone;

            Intent.PutExtra("PrintContent", false);    //Reset

            OrderLines = FindViewById<ListView>(Resource.Id.myListView);
            OrderLines.ItemClick += ListView_ItemClick;

            Button showPopupMenu = FindViewById<Button>(Resource.Id.PopupMenuPallet);
            showPopupMenu.Click += (s, arg) =>
            {
                PopupMenu menu = new PopupMenu(this, showPopupMenu);
                menu.Inflate(Resource.Menu.PopupMenuCallOffPallet);
                menu.Show();

                menu.MenuItemClick += (s1, arg1) =>
                {
                    switch (arg1.Item.TitleFormatted.ToString())
                    {
                        case "New":
                            {
                                NewPallet();
                                break;
                            }
                        case "Find":
                            {
                                CheckPallets();
                                break;
                            }
                        case "Change Type":
                            {
                                ChangePallet();
                                break;
                            }
                        case "Wrap":
                            {
                                WrapPallet();
                                break;
                            }
                        case "Move":
                            {
                                MovePallet();
                                break;
                            }
                        case "Count":
                            {
                                CountBin();
                                break;
                            }
                        case "Log":
                            {
                                ShowLog();
                                break;
                            }
                    }
                };
            };

            btnPrintContent = FindViewById<Button>(Resource.Id.btnPrintContent);
            btnPrintContent.Click += OnPrintContent;

            tvCustomerNo = FindViewById<TextView>(Resource.Id.tvCustomerNo);
            tvCustomerName = FindViewById<TextView>(Resource.Id.tvCustomerName);
            tvPalletID = FindViewById<TextView>(Resource.Id.tvPalletID);
            cbPalletFree = FindViewById<CheckBox>(Resource.Id.cbPalletFree);

            tvCustomerNo.Text = Helper.CurrentCustomerNo;
            tvCustomerName.Text = Helper.CurrentCustomerName;
            tvPalletID.Text = Helper.CurrentPallet;
            cbPalletFree.Checked = Helper.CurrentPalletFree;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {
            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                OrderLines.Adapter = new AdapterOrderLines(this, orderItems);
            }
            else
            {
                Helper.ShowToast(this, ResponseMassage, true);
                Finish();
            }
            pbProgress.Visibility = ViewStates.Gone;
            LLAll.Visibility = ViewStates.Visible;
        }

        private Boolean LoadData()
        {
            OrderClass.Lines.Clear();
            try
            {
                if (Helper.Demo)
                {
                    OrderClass.Lines.Add(10000, new OrderLine
                    {
                        Description = "Demo item 1",
                        InnerOuter = "Outer",
                        InnerOuterQty = Convert.ToDecimal(1),
                        ItemNo = "1010",
                        LineNo = 10000,
                        PickBin = "103-1234-00",
                        Quantity = Convert.ToDecimal(10),
                        ShowColor = true
                    });
                    OrderClass.Lines.Add(20000, new OrderLine
                    {
                        Description = "Demo item 2",
                        InnerOuter = "Outer",
                        InnerOuterQty = Convert.ToDecimal(5),
                        ItemNo = "2020",
                        LineNo = 20000,
                        PickBin = "101-1234-00",
                        Quantity = Convert.ToDecimal(25),
                        ShowColor = false
                    });
                    OrderClass.Lines.Add(30000, new OrderLine
                    {
                        Description = "Demo item 3",
                        InnerOuter = "Inner",
                        InnerOuterQty = Convert.ToDecimal(2),
                        ItemNo = "2030",
                        LineNo = 30000,
                        PickBin = "101-1134-00",
                        Quantity = Convert.ToDecimal(8),
                        ShowColor = true
                    });
                    OrderClass.Lines.Add(40000, new OrderLine
                    {
                        Description = "Demo item 4",
                        InnerOuter = "Outer",
                        InnerOuterQty = Convert.ToDecimal(10),
                        ItemNo = "5010",
                        LineNo = 40000,
                        PickBin = "105-0214-00",
                        Quantity = Convert.ToDecimal(2),
                        ShowColor = false
                    });
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    activePickingLines_root ResponseRoot = new activePickingLines_root();

                    ws.OpenCallOffOrder(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentCustomerNo);

                    activePickingLines_response XmlResponse = ResponseRoot.activePickingLines_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        bool LineColor = false;
                        orderItems = new List<OrderLine>(XmlResponse.activePickingLines.Length);
                        foreach (activePickingLines item in XmlResponse.activePickingLines)
                        {
                            LineColor = !LineColor;
                            OrderClass.Lines.Add(item.lineNo, new OrderLine
                            {
                                Description = item.description,
                                InnerOuter = item.innerOuter,
                                InnerOuterQty = Helper.FormatTextDecimal(item.innerOuterQty),
                                ItemNo = item.itemNo,
                                LineNo = item.lineNo,
                                PickBin = item.pickBin,
                                Quantity = Helper.FormatTextDecimal(item.quantity),
                                InOutFormat = item.InOutFormat,
                                ShowColor = LineColor
                            });
                        }
                    }
                    else
                    {
                        ResponseTitle = XmlResponse.status;
                        ResponseMassage = XmlResponse.status_text;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ResponseTitle = "ERROR";
                ResponseMassage = ex.Message;
                return false;
            }
            orderItems.Clear();
            orderItems.AddRange(OrderClass.Lines.Values);
            return true;
        }

        private void OnPrintContent(object sender, EventArgs e)
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.ShowToast(this, "Printed", false);
                    Helper.CurrentPallet = "";
                    Helper.CurrentPalletFree = false;
                    tvPalletID.Text = "";
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root ResponseRoot = new default_root();

                    ws.PrintContent(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentPallet);

                    default_response XmlResponse = ResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.ShowToast(this, XmlResponse.status_text, false);
                        Helper.CurrentPallet = "";
                        Helper.CurrentPalletFree = false;
                        tvPalletID.Text = "";
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void WrapPallet()
        {
            Helper.CloseActivity = true;
            Intent intent = new Intent(this, typeof(WrapPallet));
            StartActivityForResult(intent, 2);
        }

        private void MovePallet()
        {
            Helper.CloseActivity = true;
            Intent intent = new Intent(this, typeof(MovePallet));
            StartActivityForResult(intent, 5);
        }

        private void CountBin()
        {
            Helper.CloseActivity = true;
            Intent intent = new Intent(this, typeof(MenuCount));
            StartActivityForResult(intent, 3);
        }

        private void ShowLog()
        {
            Helper.CloseActivity = true;
            Intent intent = new Intent(this, typeof(ActivityLogList));
            StartActivityForResult(intent, 4);
        }

        private void ChangePallet()
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.ShowToast(this, "Type changed", false);
                    Helper.CurrentPalletFree = !Helper.CurrentPalletFree;
                    cbPalletFree.Checked = Helper.CurrentPalletFree;
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root ResponseRoot = new default_root();

                    ws.ChangePalletType(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentPallet);

                    default_response XmlResponse = ResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.ShowToast(this, XmlResponse.status_text, false);
                        Helper.CurrentPalletFree = !Helper.CurrentPalletFree;
                        cbPalletFree.Checked = Helper.CurrentPalletFree;
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void FindPallet()
        {
            Intent intent = new Intent(this, typeof(SelectPallet));
            StartActivityForResult(intent, 1);
        }

        private void NewPallet()
        {
            try
            {
                if (Helper.Demo)
                {
                    Helper.CurrentPallet = "50026";
                    tvPalletID.Text = Helper.CurrentPallet;
                    Helper.CurrentPalletFree = false;
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root ResponseRoot = new default_root();

                    ws.NewPallet(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentCustomerNo, 1);

                    default_response XmlResponse = ResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.CurrentPallet = XmlResponse.status_text;
                        tvPalletID.Text = Helper.CurrentPallet;
                        Helper.CurrentPalletFree = false;
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            PickLineNo = e.Position;
            if (Helper.CurrentPallet == "")
            {
                var options = new[] { "New", "Find" };
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.SetTitle("Which Pallet to use?");
                builder.SetItems(options, ((innersender, innerargs) =>
                {
                    switch (innerargs.Which)
                    {
                        case 0:
                            {
                                NewPallet();
                                PickLine(PickLineNo);
                                break;
                            }
                        case 1:
                            {
                                CheckPallets();
                                break;
                            }
                    }
                }));
                builder.Show();
            }
            else
            {
                PickLine(PickLineNo);
            }
        }

        private void CheckPallets()
        {
            try
            {
                if (Helper.Demo)
                {
                    FindPallet();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    pallet_root ResponseRoot = new pallet_root();

                    ws.GetStoresPallets(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentCustomerNo, 0);

                    pallet_response XmlResponse = ResponseRoot.pallet_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        FindPallet();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowToast(this, ex.Message, true);
            }
        }

        private void PickLine(int LineNo)
        {
            OrderLine Item = OrderLines.GetItemAtPosition(LineNo).Cast<OrderLine>();
            Intent intent = new Intent(this, typeof(PickCallOffItem));
            Helper.CurrentLineNo = Item.LineNo;
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 0:
                    {
                        if (resultCode == Result.Ok)
                        {
                            if (data.GetBooleanExtra("PrintContent", false))
                            { btnPrintContent.CallOnClick(); }

                            if (OrderClass.Lines.Count == 0)
                            { Finish(); }
                        }
                        Recreate();
                        break;
                    }
                case 1:
                    {
                        if (Helper.CurrentPallet != "")
                        {
                            PickLine(PickLineNo);
                        }
                        break;
                    }
            }            
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("Release order");
            dialog.SetMessage("Do you want to release this order?");
            dialog.SetPositiveButton("OK", OkAction);
            dialog.SetNegativeButton("Cancel", CancelAction);
            var myCustomDialog = dialog.Create();
            myCustomDialog.Show();
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            try
            {
                if (Helper.Demo)
                {
                    Finish();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root ResponseRoot = new default_root();

                    ws.UnlockCallOffOrder(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentCustomerNo);

                    default_response XmlResponse = ResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Finish();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void CancelAction(object sender, DialogClickEventArgs e)
        {
            //Do nothing
        }
    }
}