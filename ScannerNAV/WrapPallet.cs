﻿using System;
using System.Collections.Generic;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class WrapPallet : Activity
    {
        private wraptype[] wraptypeList;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            LoadView();
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.WrapPallet);

            FindViewById<EditText>(Resource.Id.etPalletNo).Text = "";
            FindViewById<EditText>(Resource.Id.etBin).Text = "";

            FindViewById<EditText>(Resource.Id.etPalletNo).RequestFocus();
            FindViewById<EditText>(Resource.Id.etBin).KeyPress += OnBin_KeyPress;
            FindViewById<EditText>(Resource.Id.etBin).ShowSoftInputOnFocus = true;
            FindViewById<Button>(Resource.Id.btnWrap).Click += OnWrapClick;
            FindViewById<CheckBox>(Resource.Id.cbPrint).Checked = true;
            FindViewById<TextView>(Resource.Id.tvPrint).Visibility = ViewStates.Invisible;
            FindViewById<CheckBox>(Resource.Id.cbPrint).Visibility = ViewStates.Invisible;
            FindViewById<TextView>(Resource.Id.tvWrapType).Visibility = ViewStates.Invisible;
            FindViewById<Spinner>(Resource.Id.spnWrapType).Visibility = ViewStates.Invisible;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnWrap).PerformClick();
                FindViewById<EditText>(Resource.Id.etPalletNo).RequestFocus();
            }
            else
                e.Handled = false;
        }

        private void OnWrapClick(object sender, EventArgs e)
        {
            try
            {
                if (Helper.Demo)
                {
                    if (FindViewById<TextView>(Resource.Id.tvWrapType).Visibility == ViewStates.Invisible)
                    {
                        List<string> items = new List<string>(6)
                        {
                            "01" + " - " + "Wrap 1",
                            "02" + " - " + "Wrap 2",
                            "03" + " - " + "Wrap 3",
                            "04" + " - " + "Wrap 4",
                            "05" + " - " + "Wrap 5"
                        };
                        ArrayAdapter adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                        FindViewById<Spinner>(Resource.Id.spnWrapType).Adapter = adapter;
                        FindViewById<Spinner>(Resource.Id.spnWrapType).SetSelection(3);
                        FindViewById<TextView>(Resource.Id.tvWrapType).Visibility = ViewStates.Visible;
                        FindViewById<Spinner>(Resource.Id.spnWrapType).Visibility = ViewStates.Visible;
                        FindViewById<Spinner>(Resource.Id.spnWrapType).RequestFocus();
                    }
                    else
                    {
                        Helper.ShowToast(this, $"Pallet {FindViewById<EditText>(Resource.Id.etPalletNo).Text} moved", false);
                        if (Helper.CloseActivity)
                        {
                            Helper.CloseActivity = false;
                            Finish();
                        }
                        else
                        {
                            LoadView();
                        }
                    }
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    wraptype_root navResponse = new wraptype_root();

                    if (FindViewById<TextView>(Resource.Id.tvWrapType).Visibility == ViewStates.Invisible)
                    {
                        ws.WrapPallet(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etPalletNo).Text, FindViewById<EditText>(Resource.Id.etBin).Text, FindViewById<CheckBox>(Resource.Id.cbPrint).Checked, "0");
                    }
                    else
                    {
                        ws.WrapPallet(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etPalletNo).Text, FindViewById<EditText>(Resource.Id.etBin).Text, FindViewById<CheckBox>(Resource.Id.cbPrint).Checked, wraptypeList[FindViewById<Spinner>(Resource.Id.spnWrapType).SelectedItemPosition].wraptype_entryno.ToString());
                    }

                    wraptype_response wraptypeXmlResponse = navResponse.wraptype_response[0];

                    if (Helper.IsOK(wraptypeXmlResponse.status))
                    {
                        Helper.ShowToast(this, wraptypeXmlResponse.status_text, false);
                        if (Helper.CloseActivity)
                        {
                            Helper.CloseActivity = false;
                            Finish();
                        }
                        else
                        {
                            LoadView();
                        }
                    }
                    else if (Helper.IsNeddInfo(wraptypeXmlResponse.status))
                    {
                        int counter = 0;
                        int selectedItem = 0;
                        List<string> items = new List<string>(wraptypeXmlResponse.wraptype.Length);
                        wraptypeList = wraptypeXmlResponse.wraptype;
                        foreach (wraptype item in wraptypeXmlResponse.wraptype)
                        {
                            items.Add(item.wraptype_entryno + " - " + item.wraptype_desc);
                            if (item.wraptype_entryno.ToString() == wraptypeXmlResponse.default_wraptype)
                            {
                                selectedItem = counter;
                            }
                            counter++;
                        }
                        ArrayAdapter adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                        FindViewById<Spinner>(Resource.Id.spnWrapType).Adapter = adapter;
                        FindViewById<Spinner>(Resource.Id.spnWrapType).SetSelection(selectedItem);
                        FindViewById<TextView>(Resource.Id.tvWrapType).Visibility = ViewStates.Visible;
                        FindViewById<Spinner>(Resource.Id.spnWrapType).Visibility = ViewStates.Visible;
                        FindViewById<Spinner>(Resource.Id.spnWrapType).RequestFocus();
                    }
                    else
                    {
                        Helper.ShowAlertDialog(this, wraptypeXmlResponse.status, wraptypeXmlResponse.status_text);
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }
    }
}