﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Menu : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Menu);

            FindViewById<Button>(Resource.Id.btnCreatePallet).Click += OnCreatePallet_Click;
            FindViewById<Button>(Resource.Id.btnMovePallet).Click += OnMovePallet_Click;
            FindViewById<Button>(Resource.Id.btnPickOrder).Click += OnPickOrder_Click;
            FindViewById<Button>(Resource.Id.btnWrapPallet).Click += OnWrapPallet_Click;
            FindViewById<Button>(Resource.Id.btnRemovePushPallet).Click += OnRemovePushPallet_Click;
            FindViewById<Button>(Resource.Id.btnCount).Click += OnCount_Click;
            FindViewById<Button>(Resource.Id.btnOtherFunctions).Click += OtherFunctions_Click;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();

            // Add version to the page
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);
        }

        public void OnPickOrder_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(MenuPick));
        }

        private void OnMovePallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(MovePallet));
        }

        private void OnCreatePallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(CreatePallet));
        }

        private void OnRemovePushPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(MenuRemovePallet));
        }

        private void OnWrapPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(WrapPallet));
        }

        private void OnCount_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(MenuCount));
        }

        private void OtherFunctions_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(Menu2));
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.SetTitle("Log off");
            dialog.SetMessage("Do you want to log off?");
            dialog.SetPositiveButton("OK", OkAction);
            dialog.SetNegativeButton("Cancel", CancelAction);
            var myCustomDialog = dialog.Create();
            myCustomDialog.Show();
        }

        private void OkAction(object sender, DialogClickEventArgs e)
        {
            base.OnBackPressed();
        }

        private void CancelAction(object sender, DialogClickEventArgs e)
        {
            //Do nothing
        }
    }
}