﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "PickPushOrderList", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class PickNewStoreOrderList : Activity
    {
        private ProgressBar pbProgress;
        private String ResponseTitle = "";
        private String ResponseMassage = "";
        private LinearLayout LLAll;
        private List<Order> orderItems = new List<Order>();
        private ListView Orders;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.PickOrderList);

            pbProgress = FindViewById<ProgressBar>(Resource.Id.pbProgress);
            pbProgress.Visibility = ViewStates.Visible;
            LLAll = FindViewById<LinearLayout>(Resource.Id.LLAll);
            LLAll.Visibility = ViewStates.Gone;

            Orders = FindViewById<ListView>(Resource.Id.listViewOrders);
            Orders.ItemClick += OrderLines_ItemClick;

            Helper.CurrentPallet = "";
            Helper.CurrentPalletFree = false;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {
            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                Orders.Adapter = new AdapterOrderOrders(this, orderItems);
            }
            else
            {
                Helper.ShowAlertDialog(this, ResponseTitle, ResponseMassage);
            }

            if (ResponseMassage != "")
            {
                Helper.ShowToast(this, ResponseMassage, true);
                Finish();
            }

            pbProgress.Visibility = ViewStates.Gone;
            LLAll.Visibility = ViewStates.Visible;
        }

        private Boolean LoadData()
        {
            OrderClass.Orders.Clear();
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                order_root ResponseRoot = new order_root();

                ws.GetNewStoreOrders(ref ResponseRoot, Helper.RescourceNo, false);

                order_response XmlResponse = ResponseRoot.order_response[0];

                ResponseMassage = XmlResponse.status_text;

                if (Helper.IsOK(XmlResponse.status))
                {
                    bool LineColor = false;
                    orderItems = new List<Order>(XmlResponse.order.Length);
                    foreach (order item in XmlResponse.order)
                    {
                        LineColor = !LineColor;
                        OrderClass.Orders.Add(item.customer, new Order
                        {
                            CustomerNo = item.customer,
                            CustomerName = item.customerName,
                            WhoisPicking = item.whoIsPicking,
                            ShowColor = LineColor
                        });
                    }
                }
                else
                {
                    ResponseTitle = XmlResponse.status;                    
                    return false;
                }
                orderItems.Clear();
                orderItems.AddRange(OrderClass.Orders.Values);
            }
            catch (Exception ex)
            {
                ResponseTitle = "ERROR";
                ResponseMassage = ex.Message;
                return false;
            }
            return true;
        }

        private void OrderLines_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            Order Item = Orders.GetItemAtPosition(e.Position).Cast<Order>();
            Intent intent = new Intent(this, typeof(PickNewStoreOrder));
            Helper.CurrentCustomerNo = Item.CustomerNo;
            Helper.CurrentCustomerName = Item.CustomerName;
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            Recreate();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}