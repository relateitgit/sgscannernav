﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Widget;
using Android.Views;

namespace ScannerNAV
{
    [Activity(Label = "SettingsConnect", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SettingsConnect : Activity
    {
        private List<string> Items;
        private int System;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.SettingsConnect);

            System = Convert.ToInt32(Helper.MySystem);

            FindViewById<TextView>(Resource.Id.tvUsername).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etUsername).Text = Helper.WSUser;
            FindViewById<EditText>(Resource.Id.etUsername).Visibility = ViewStates.Invisible;
            FindViewById<TextView>(Resource.Id.tvPassword).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etPassword).Text = Helper.WSPassword;
            FindViewById<EditText>(Resource.Id.etPassword).Visibility = ViewStates.Invisible;
            FindViewById<TextView>(Resource.Id.tvUrl).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etUrl).Text = Helper.WSUrl;
            FindViewById<EditText>(Resource.Id.etUrl).Visibility = ViewStates.Invisible;

            FindViewById<Button>(Resource.Id.btnSave).Click += OnSave;

            FindViewById<Spinner>(Resource.Id.spnSystem).ItemSelected += SettingsConnect_ItemSelected;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {

            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                FindViewById<Spinner>(Resource.Id.spnSystem).Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, Items);
                FindViewById<Spinner>(Resource.Id.spnSystem).SetSelection(System);
            }
            else
            {
                Helper.ShowAlertDialog(this, "ERROR", "Can't add systems");
            }

            FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Visible;
        }

        private bool LoadData()
        {
            Items = new List<string>() { "Production", "Test", "Custom", "Demo" };
            return true;
        }

        private void SettingsConnect_ItemSelected(object sender, AdapterView.ItemSelectedEventArgs e)
        {
            Spinner spinner = (Spinner)sender;
            if (spinner.SelectedItemPosition.ToString() == "2")
            {
                FindViewById<EditText>(Resource.Id.etUsername).Visibility = ViewStates.Visible;
                FindViewById<EditText>(Resource.Id.etPassword).Visibility = ViewStates.Visible;
                FindViewById<EditText>(Resource.Id.etUrl).Visibility = ViewStates.Visible;
                FindViewById<TextView>(Resource.Id.tvUsername).Visibility = ViewStates.Visible;
                FindViewById<TextView>(Resource.Id.tvPassword).Visibility = ViewStates.Visible;
                FindViewById<TextView>(Resource.Id.tvUrl).Visibility = ViewStates.Visible;
            }
            else
            {
                FindViewById<EditText>(Resource.Id.etUsername).Visibility = ViewStates.Invisible;
                FindViewById<EditText>(Resource.Id.etPassword).Visibility = ViewStates.Invisible;
                FindViewById<EditText>(Resource.Id.etUrl).Visibility = ViewStates.Invisible;
                FindViewById<TextView>(Resource.Id.tvUsername).Visibility = ViewStates.Invisible;
                FindViewById<TextView>(Resource.Id.tvPassword).Visibility = ViewStates.Invisible;
                FindViewById<TextView>(Resource.Id.tvUrl).Visibility = ViewStates.Invisible;
            }
        }

        private void OnSave(object sender, EventArgs e)
        {
            Helper.MySystem = FindViewById<Spinner>(Resource.Id.spnSystem).SelectedItemPosition.ToString();
            SetResult(Result.Ok);
            Finish();
        }
    }
}