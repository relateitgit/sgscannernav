﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Add Push Pallet", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MovePallet : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            LoadView();
        }

        private void LoadView()
        {
            SetContentView(Resource.Layout.MovePallet);

            FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Visible;

            FindViewById<EditText>(Resource.Id.etPalletNo).Text = "";
            FindViewById<EditText>(Resource.Id.etNewBin).Text = "";
            FindViewById<EditText>(Resource.Id.etLane).Text = "0";

            FindViewById<EditText>(Resource.Id.etPalletNo).RequestFocus();
            FindViewById<EditText>(Resource.Id.etPalletNo).KeyPress += OnPallet_KeyPress;
            FindViewById<TextView>(Resource.Id.tvNewBin).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etNewBin).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etNewBin).KeyPress += OnBin_KeyPress;
            FindViewById<TextView>(Resource.Id.tvLane).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etLane).Visibility = ViewStates.Invisible;
            FindViewById<EditText>(Resource.Id.etLane).KeyPress += OnLane_KeyPress;
            FindViewById<Button>(Resource.Id.btnAddToPush).Click += OnMoveClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnPallet_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Visible;
                FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Gone;
                try
                {
                    if (Helper.Demo)
                    {
                        FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Gone;
                        FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Visible;

                        FindViewById<TextView>(Resource.Id.tvNewBin).Visibility = ViewStates.Visible;
                        FindViewById<EditText>(Resource.Id.etNewBin).Visibility = ViewStates.Visible;
                    }
                    else
                    {
                        ScannerInterface ws = Helper.GetInterface(this);

                        default_root navResponse = new default_root();

                        ws.GetWrapOutBin(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etPalletNo).Text);

                        default_response XmlResponse = navResponse.default_response[0];

                        FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Gone;
                        FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Visible;

                        if (Helper.IsOK(XmlResponse.status))
                        {
                            if (XmlResponse.status_text != "")
                            {
                                Helper.CurrentPallet = FindViewById<EditText>(Resource.Id.etPalletNo).Text;
                                FindViewById<EditText>(Resource.Id.etNewBin).Text = XmlResponse.status_text;
                            }

                            FindViewById<TextView>(Resource.Id.tvNewBin).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etNewBin).Visibility = ViewStates.Visible;
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                }
            }
            else
                e.Handled = false;
        }

        private void OnBin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter && !Helper.CheckInput)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnAddToPush).PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnLane_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnAddToPush).PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnMoveClick(object sender, EventArgs e)
        {
            try
            {
                if (FindViewById<EditText>(Resource.Id.etNewBin).Text != "")
                {
                    if (Helper.Demo)
                    {
                        if (FindViewById<EditText>(Resource.Id.etLane).Text == "0")
                        {
                            FindViewById<TextView>(Resource.Id.tvLane).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etLane).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etLane).Text = "";
                        }
                        else
                        {
                            Helper.ShowToast(this, $"Pallet {FindViewById<EditText>(Resource.Id.etPalletNo).Text} moved", false);
                            LoadView();
                        }
                    }
                    else
                    {
                        ScannerInterface ws = Helper.GetInterface(this);

                        default_root navResponse = new default_root();

                        ws.MovePallet(ref navResponse, Helper.RescourceNo, FindViewById<EditText>(Resource.Id.etPalletNo).Text, FindViewById<EditText>(Resource.Id.etNewBin).Text, Convert.ToInt32(FindViewById<EditText>(Resource.Id.etLane).Text));

                        default_response XmlResponse = navResponse.default_response[0];

                        if (Helper.IsNeddInfo(XmlResponse.status))
                        {
                            FindViewById<TextView>(Resource.Id.tvLane).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etLane).Visibility = ViewStates.Visible;
                            FindViewById<EditText>(Resource.Id.etLane).Text = "";
                        }
                        else if (Helper.IsOK(XmlResponse.status))
                        {
                            Helper.ShowToast(this, XmlResponse.status_text, false);
                            LoadView();
                        }
                        else
                        {
                            Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);

            switch (requestCode)
            {
                case 0:
                    {
                        FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
                        break;
                    }
                case 1:
                    {
                        Finish();
                        break;
                    }
            }
        }
    }
}