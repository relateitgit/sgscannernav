﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class Menu2 : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Menu2);
            
            FindViewById<Button>(Resource.Id.btnPrintPallet).Click += OnPrintPallet_Click;
            FindViewById<Button>(Resource.Id.btnRepackPallet).Click += OnRepackPallet_Click;
            FindViewById<Button>(Resource.Id.btnLoadingPallet).Click += OnLoadingPallet_Click;
            FindViewById<Button>(Resource.Id.btnFindItemLocation).Click += OnFinItemLocation_Click;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();

            // Add version to the page
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);

            if (Helper.Demo)
            {
                FindViewById<Button>(Resource.Id.btnPrintPallet).Visibility = ViewStates.Invisible;
                FindViewById<Button>(Resource.Id.btnLoadingPallet).Visibility = ViewStates.Invisible;
            }
            FindViewById<Button>(Resource.Id.btnTemplate2).Visibility = ViewStates.Invisible;
            FindViewById<Button>(Resource.Id.btnTemplate3).Visibility = ViewStates.Invisible;
            FindViewById<Button>(Resource.Id.btnTemplate4).Visibility = ViewStates.Invisible;
        }              

        private void OnPrintPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(PrintPallet));
        }

        private void OnRepackPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(Repacking));
        }

        private void OnLoadingPallet_Click(object sender, EventArgs e)
        {
            //StartActivity(typeof(LoadingPallet));
            throw new NotImplementedException();
        }

        private void OnFinItemLocation_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(FindItemLocation));
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}