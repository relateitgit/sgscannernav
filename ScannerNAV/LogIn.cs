﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "LogInActivity", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class LogIn : Activity
    {
        private String ResponseTitle = "";
        private String ResponseMassage = "";
        private int selectedItem = 0;
        private resource[] resourceList;
        private List<string> items;        

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.LogIn);

            FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Visible;
            FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Gone;

            FindViewById<TextView>(Resource.Id.etPIN).KeyPress += OnPin_KeyPress;
            FindViewById<TextView>(Resource.Id.etPIN).RequestFocus();

            FindViewById<Button>(Resource.Id.btnLogin).Click += OnLoginClick;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {
            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                FindViewById<Spinner>(Resource.Id.spnResource).Adapter = new ArrayAdapter(this, Android.Resource.Layout.SimpleSpinnerDropDownItem, items);
                FindViewById<Spinner>(Resource.Id.spnResource).SetSelection(selectedItem);
            }
            else
            {
                Helper.ShowAlertDialog(this, ResponseTitle, ResponseMassage);
            }

            FindViewById<ProgressBar>(Resource.Id.pbProgress).Visibility = ViewStates.Gone;
            FindViewById<LinearLayout>(Resource.Id.LLAll).Visibility = ViewStates.Visible;

            InputMethodManager inputMethodManager = this.FindViewById<TextView>(Resource.Id.etPIN).Context.GetSystemService(Android.Content.Context.InputMethodService) as InputMethodManager;
            inputMethodManager.ShowSoftInput(this.FindViewById<TextView>(Resource.Id.etPIN), ShowFlags.Forced);
            inputMethodManager.ToggleSoftInput(ShowFlags.Forced, HideSoftInputFlags.ImplicitOnly);
        }

        private Boolean LoadData()
        {
            try
            {
                if (Helper.Demo)
                {
                         items = new List<string>(5)
                    {
                        "DEMO1" + " - " + "Demobruger 1",
                        "DEMO2" + " - " + "Demobruger 2",
                        "DEMO3" + " - " + "Demobruger 3",
                        "DEMO4" + " - " + "Demobruger 4",
                        "DEMO5" + " - " + "Demobruger 5"
                    };
                    selectedItem = 3;
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    resource_root resourceResponseRoot = new resource_root();

                    ws.GetResources(ref resourceResponseRoot);

                    resource_response XmlResponse = resourceResponseRoot.resource_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        int counter = 0;
                        string ResourceNo = Helper.RescourceNo;
                        items = new List<string>(XmlResponse.resource.Length);
                        resourceList = XmlResponse.resource;
                        foreach (resource item in XmlResponse.resource)
                        {
                            items.Add(item.no + " - " + item.name);
                            if (item.no == ResourceNo)
                            {
                                selectedItem = counter;
                            }
                            counter++;
                        }
                    }
                    else
                    {
                        ResponseTitle = XmlResponse.status;
                        ResponseMassage = XmlResponse.status_text;
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                ResponseTitle = "ERROR";
                ResponseMassage = ex.Message;
                return false;
            }
            return true;
        }

        protected void OnPin_KeyPress(object sender, View.KeyEventArgs e)
        {
            if ((e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter) || (FindViewById<TextView>(Resource.Id.etPIN).Text.Length == 4))
            {
                e.Handled = true;
                FindViewById<Button>(Resource.Id.btnLogin).PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnLoginClick(object sender, System.EventArgs e)
        {
            InputMethodManager inputMethodManager = this.FindViewById<TextView>(Resource.Id.etPIN).Context.GetSystemService(Android.Content.Context.InputMethodService) as InputMethodManager;
            inputMethodManager.HideSoftInputFromWindow(FindViewById<TextView>(Resource.Id.etPIN).WindowToken, 0);
            try
            {
                if (Helper.Demo)
                {
                    Helper.RescourceNo = items[FindViewById<Spinner>(Resource.Id.spnResource).SelectedItemPosition].Substring(0, 5);
                    SetResult(Result.Ok);
                    Finish();
                }
                else
                {
                    ScannerInterface ws = Helper.GetInterface(this);

                    default_root defaultResponseRoot = new default_root();

                    ws.Login(ref defaultResponseRoot, resourceList[FindViewById<Spinner>(Resource.Id.spnResource).SelectedItemPosition].no, Convert.ToInt32(FindViewById<TextView>(Resource.Id.etPIN).Text));

                    default_response XmlResponse = defaultResponseRoot.default_response[0];

                    if (Helper.IsOK(XmlResponse.status))
                    {
                        Helper.RescourceNo = resourceList[FindViewById<Spinner>(Resource.Id.spnResource).SelectedItemPosition].no;
                        SetResult(Result.Ok);
                        Finish();
                    }
                    else
                    {                        
                        Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                        FindViewById<TextView>(Resource.Id.etPIN).Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
                FindViewById<TextView>(Resource.Id.etPIN).Text = "";
            }
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            SetResult(Result.Canceled);
            Finish();
        }
    }
}