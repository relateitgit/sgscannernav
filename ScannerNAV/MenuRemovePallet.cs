﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MenuRemovePallet : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.MenuRemovePallet);

            FindViewById<Button>(Resource.Id.btnEmptyPallet).Click += OnEmptyPallet_Click;

            FindViewById<Button>(Resource.Id.btnPalletWithContent).Click += OnPalletWithContent_Click;

            FindViewById<Button>(Resource.Id.btnErrorPallet).Click += OnErrorPallet_Click;

            FindViewById<Button>(Resource.Id.btnPrePallets).Click += OnRemovePrePallet_Click;

            FindViewById<Button>(Resource.Id.btnEmptyBin).Click += OnEmptyBin_Click;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();

            // Add version to the page
            FindViewById<TextView>(Resource.Id.textViewVersion).Text = Helper.GetVersion(this);

            if (Helper.Demo)
            {
                FindViewById<Button>(Resource.Id.btnEmptyPallet).Visibility = ViewStates.Invisible;
                FindViewById<Button>(Resource.Id.btnPalletWithContent).Visibility = ViewStates.Invisible;
                FindViewById<Button>(Resource.Id.btnErrorPallet).Visibility = ViewStates.Invisible;
                FindViewById<Button>(Resource.Id.btnPrePallets).Visibility = ViewStates.Invisible;
            }
        }

        private void OnRemovePrePallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(RemovePrePallet));
        }

        private void OnEmptyPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(RemoveEmptyPushPallet));
        }

        private void OnPalletWithContent_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(RemovePalletWithContent));
        }

        private void OnErrorPallet_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(RemoveErrorPallet));
        }

        private void OnEmptyBin_Click(object sender, EventArgs e)
        {
            StartActivity(typeof(RemoveAll));
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}