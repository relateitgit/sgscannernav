﻿using System;
using Android.App;
using Android.OS;
using ScannerNAV.Webservice;
using Android.Views;
using Android.Widget;
using Android.Content;

namespace ScannerNAV
{
    [Activity(Label = "Remove Push Pallet", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class RemovePalletWithContent : Activity
    {
        private Button btnRemove;
        private EditText etPalletNo;
        private EditText etBin;
        private EditText etBarcode;
        private EditText etQty;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Create your application here
            SetContentView(Resource.Layout.RemovePalletWithContent);

            etPalletNo = FindViewById<EditText>(Resource.Id.etPalletNo);
            etBin = FindViewById<EditText>(Resource.Id.etBin);
            etBarcode = FindViewById<EditText>(Resource.Id.etBarcode);
            etQty = FindViewById<EditText>(Resource.Id.etQty);
            etQty.KeyPress += OnQty_KeyPress;

            btnRemove = FindViewById<Button>(Resource.Id.btnRemove);
            btnRemove.Click += OnRemoveClik;

            FindViewById<TextView>(Resource.Id.tvStatus).Click += OnStatusClick;
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        private void OnQty_KeyPress(object sender, View.KeyEventArgs e)
        {
            if (e.Event.Action == KeyEventActions.Down && e.KeyCode == Keycode.Enter)
            {
                e.Handled = true;
                btnRemove.PerformClick();
            }
            else
                e.Handled = false;
        }

        private void OnRemoveClik(object sender, EventArgs e)
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                default_root navResponse = new default_root();

                ws.RemovePalletWithContentFromPush(ref navResponse, Helper.RescourceNo, etPalletNo.Text, etBin.Text, etBarcode.Text, Helper.FormatDecimal(Convert.ToDecimal(etQty.Text)));

                default_response XmlResponse = navResponse.default_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    Helper.ShowToast(this, XmlResponse.status_text, false);
                    etBin.Text = "";
                    Finish();
                }
                else
                {
                    Helper.ShowAlertDialog(this, XmlResponse.status, XmlResponse.status_text);
                }
            }
            catch (Exception ex)
            {
                Helper.ShowAlertDialog(this, "ERROR", ex.Message);
            }
        }

        private void OnStatusClick(object sender, EventArgs e)
        {
            Intent intent = new Intent(this, typeof(Settings));
            StartActivityForResult(intent, 0);
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            FindViewById<TextView>(Resource.Id.tvStatus).Text = Helper.GetStatus();
        }

        override public void OnBackPressed()
        {
            // do something here and don't write super.onBackPressed()
            Finish();
        }
    }
}