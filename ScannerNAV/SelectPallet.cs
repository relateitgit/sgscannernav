﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using ScannerNAV.Webservice;

namespace ScannerNAV
{
    [Activity(Label = "ScannerNAV", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class SelectPallet : Activity
    {
        private ProgressBar pbProgress;
        private String ResponseTitle = "";
        private String ResponseMassage = "";
        private LinearLayout LLAll;
        private List<StorePallet> palletItems = new List<StorePallet>();

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            RequestWindowFeature(WindowFeatures.NoTitle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.SelectPallet);

            pbProgress = FindViewById<ProgressBar>(Resource.Id.pbProgress);
            pbProgress.Visibility = ViewStates.Visible;
            LLAll = FindViewById<LinearLayout>(Resource.Id.LLAll);
            LLAll.Visibility = ViewStates.Gone;

            FindViewById<ListView>(Resource.Id.myListView).ItemClick += ListView_ItemClick;

            LoadDataAsync();
        }

        private async void LoadDataAsync()
        {
            // This method runs asynchronously.
            if (await Task.Run(() => LoadData()))
            {
                FindViewById<ListView>(Resource.Id.myListView).Adapter = new AdapterStorePallet(this, palletItems);
            }
            else
            {
                Helper.ShowAlertDialog(this, ResponseTitle, ResponseMassage);
            }
            pbProgress.Visibility = ViewStates.Gone;
            LLAll.Visibility = ViewStates.Visible;
        }

        private Boolean LoadData()
        {
            try
            {
                ScannerInterface ws = Helper.GetInterface(this);

                pallet_root ResponseRoot = new pallet_root();

                ws.GetStoresPallets(ref ResponseRoot, Helper.RescourceNo, Helper.CurrentCustomerNo, 0);

                pallet_response XmlResponse = ResponseRoot.pallet_response[0];

                if (Helper.IsOK(XmlResponse.status))
                {
                    bool LineColor = false;
                    bool ItemFreePallet = false;
                    palletItems = new List<StorePallet>(XmlResponse.pallet.Length);
                    foreach (pallet item in XmlResponse.pallet)
                    {
                        LineColor = !LineColor;
                        Boolean.TryParse(item.freePallet, out ItemFreePallet);
                        palletItems.Add(new StorePallet
                        {
                            Bin = item.bin,
                            FreePallet = ItemFreePallet,
                            Id = item.id,
                            Status = item.status,
                            StoreName = item.storeName,
                            Zone = item.zone,
                            ShowColor = LineColor
                        });
                    }
                }
                else
                {
                    ResponseTitle = XmlResponse.status;
                    ResponseMassage = XmlResponse.status_text;
                    return false;
                }
            }
            catch (Exception ex)
            {
                ResponseTitle = "ERROR";
                ResponseMassage = ex.Message;
                return false;
            }
            return true;
        }

        private void ListView_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            StorePallet Item = FindViewById<ListView>(Resource.Id.myListView).GetItemAtPosition(e.Position).Cast<StorePallet>();
            Helper.CurrentPallet = Item.Id;
            Helper.CurrentPalletFree = Item.FreePallet;
            Finish();
        }

        override public void OnBackPressed()
        {
            Finish();
        }
    }
}